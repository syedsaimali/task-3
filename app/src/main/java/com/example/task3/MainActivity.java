package com.example.task3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showToast("On Create");
        setContentView(R.layout.activity_main);
    }

    public void showToast(String val) {
        Toast toast = Toast.makeText(MainActivity.this,val,Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        showToast("On Start");
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onPause() {
        super.onPause();
        showToast("On Pause");
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showToast("On Resume");
        setContentView(R.layout.activity_main);
    }

    @Override
    public void finish() {
        super.finish();
        showToast("On Finish");
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        showToast("On Destroy");
        setContentView(R.layout.activity_main);

    }
}